# mattermost-api

[Mattermost](https://mattermost.com/) の [API](https://github.com/mattermost/mattermost-api-reference) を使ったツール作成用

## 開発環境

```
docker run \
  -it \
  --rm \
  -v $(pwd)/lib:/usr/src/app \
  -v mattermost_api_python_global:/usr/local \
  -v mattermost_api_python_user:/root/.local \
  -w /usr/src/app \
  python:3-alpine \
    pip install --no-cache-dir -r ./requirements.txt
```

## ビルド方法

```
docker rmi mattermost-api

docker build -t mattermost-api --force-rm=true .
```

## 利用方法

### Mattermost サーバ起動

```
mkdir -p $(pwd)/mnt/4.3/config

curl -L -o $(pwd)/mnt/4.3/config/config_docker.json https://raw.githubusercontent.com/mattermost/mattermost-docker-preview/4.3/config_docker.json
```

```
tools/_run_sv.sh mm43 4.3 8065 $(pwd)/mnt/4.3
```

## エクスポート

```
docker run \
  -it \
  --rm \
  -v $(pwd)/lib:/usr/src/app \
  -v mattermost_api_python_global:/usr/local \
  -v mattermost_api_python_user:/root/.local \
  -v $(pwd)/output:/tmp/output \
  -w /usr/src/app/mm_tools \
  python:3-alpine \
    python -m export 192.168.50.168 team01 admin admin \
      -p 8065 -o /tmp/output
```

## DB へのロード

```
docker run \
  -it \
  --rm \
  -v $(pwd)/lib:/usr/src/app \
  -v mattermost_api_python_global:/usr/local \
  -v mattermost_api_python_user:/root/.local \
  -v $(pwd)/output:/tmp/output \
  -w /usr/src/app/mm_tools \
  python:3-alpine \
    python -m load2db /tmp/output team01 \
      -o /tmp/output
```
