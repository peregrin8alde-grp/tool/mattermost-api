from . import load2db
import argparse

parser = argparse.ArgumentParser(description='load to db')
parser.add_argument('indir')
parser.add_argument('team_name')
parser.add_argument('-o', '--outdir', default='/tmp/output')

args = parser.parse_args()

load2db.load(args.indir, args.team_name, args.outdir)
