import os
import json
import sqlite3

def load(indir, team_name, outdir_base):
    os.makedirs(outdir_base, exist_ok=True)
    con = sqlite3.connect(outdir_base + '/posts.db')
    cur = con.cursor()

    cur.execute('DROP TABLE IF EXISTS "off-topic"')
    cur.execute('''CREATE TABLE "off-topic"
                (id TEXT PRIMARY KEY, post json)''')
    
    input_file_path = indir + '/' + team_name + '/off-topic' + '/posts.json'
    with open(input_file_path) as fp:
        posts = json.load(fp)
        # print(json.dumps(posts, sort_keys=False, indent=4, ensure_ascii=False))

        for post in posts.values():
            # print(json.dumps(post, sort_keys=False, indent=4, ensure_ascii=False))
            cur.execute('INSERT INTO "off-topic" VALUES  (?, ?)', (post['id'], json.dumps(post)))

    con.commit()

    for row in cur.execute('SELECT * FROM "off-topic"'):
        print(row)

    con.close()
