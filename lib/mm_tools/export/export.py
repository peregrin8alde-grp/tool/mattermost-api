# https://github.com/Vaelor/python-mattermost-driver
from mattermostdriver import Driver

import os
import json
import math

def _write_json(data, file_path):
    with open(file_path, 'w') as f:
        json.dump(data, f, sort_keys=False, indent=4, ensure_ascii=False)

def _export_channel(driver, channel_info, outdir):
    channel_name = channel_info['name']
    outdir_channel = outdir + '/' + channel_name
    os.makedirs(outdir_channel, exist_ok=True)

    _write_json(channel_info, outdir_channel + '/info.json')

    per_page = 200
    page_num = math.ceil(channel_info['total_msg_count'] / per_page)
    posts_all = {}
    for page in range(page_num):
        posts = driver.posts.get_posts_for_channel(channel_info['id'], { 'page': page, 'per_page': per_page })
        for post in posts['posts'].values():
            posts_all[post['id']] = post

    _write_json(posts_all, outdir_channel + '/posts.json')

def export(url, port, team_name, login_id, password, outdir_base):
    driver = Driver({
        'url': url,
        'login_id': login_id,
        'password': password,
        #'token': 'YourPersonalAccessToken',
        'scheme': 'http',
        'port': port,
        'basepath': '/api/v4',
        'auth': None,
        'timeout': 30,
        'request_timeout': None,
        'keepalive': False,
        'keepalive_delay': 5,
        'websocket_kw_args': None,
        'debug': False,
    })

    driver.login()

    os.makedirs(outdir_base, exist_ok=True)

    outdir_team = outdir_base + '/' + team_name
    os.makedirs(outdir_team, exist_ok=True)

    team_info = driver.teams.get_team_by_name(team_name)

    #print(team_info)
    _write_json(team_info, outdir_team + '/info.json')

    users = driver.users.get_users({ 'per_page': 200 })
    _write_json(users, outdir_team + '/user_infos.json')

    public_channel_infos = driver.channels.get_public_channels(team_info['id'])
    #print(public_channel_infos)
    _write_json(public_channel_infos, outdir_team + '/public_channel_infos.json')

    for channel_info in public_channel_infos:
        _export_channel(driver, channel_info, outdir_team)
