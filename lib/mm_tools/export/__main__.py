from . import export
import argparse

parser = argparse.ArgumentParser(description='export')
parser.add_argument('url')
parser.add_argument('team_name')
parser.add_argument('user')
parser.add_argument('pwd')
parser.add_argument('-p', '--port', type=int, default=8065)
parser.add_argument('-o', '--outdir', default='/tmp/output')

args = parser.parse_args()

export.export(args.url, args.port, args.team_name, args.user, args.pwd, args.outdir)
