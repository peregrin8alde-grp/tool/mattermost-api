FROM python:3-alpine

WORKDIR /usr/src/app

COPY lib ./
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

CMD [ "python" ]
