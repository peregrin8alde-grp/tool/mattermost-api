#!/bin/bash

#####
# args
#####

###
# NAME
###
NAME="$1"

###
# VER
# サーバのバージョン。利用可能なタグは https://hub.docker.com/r/mattermost/mattermost-preview/tags?page=1&ordering=last_updated
# 以下で動作確認。
#   - 4.3
#   - 5.0
###
VER="$2"

###
# PORT
###
PORT=$3

###
# MOUNTDIR
###
MOUNTDIR="$4"

#####
# before
#####
mkdir -p ${MOUNTDIR}/{mattermost-data,mysql}

#####
# main
#####
docker run \
  --name ${NAME} \
  --rm \
  -d \
  --publish ${PORT}:8065 \
  --add-host dockerhost:127.0.0.1 \
  -v ${MOUNTDIR}/config:/mm/mattermost/config \
  -v ${MOUNTDIR}/mattermost-data:/mm/mattermost/mattermost-data \
  -v ${MOUNTDIR}/mysql:/var/lib/mysql \
  mattermost/mattermost-preview:${VER}


exit 0
